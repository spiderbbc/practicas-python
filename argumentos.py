
# jugando un poco con argumentos enviados al momento de invocar el archivo .py
# script palabra reservada
# de la siguiente forma: python argumentos.py a b c
# salida >> hola mi nombre es el script:  argumentos.py
# el primer parametro enviado fue:  a
# el segundo parametro enviado fue:  b
# y por ultimo el tercer parametro fue:  c
from sys import argv # argv variable en la cual guarda argumentos de los cuales el archivo .py utilizara cuando corra el script

script, first, second, third = argv

print " hola mi nombre es el script: ", script
print " el primer parametro enviado fue: " , first
print " el segundo parametro enviado fue: ", second
print " y por ultimo el tercer parametro fue: ",third